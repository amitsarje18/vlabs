﻿> This experiment is taken from the Computer Graphics Lab


[Experiment link ](http://vlabs.iitb.ac.in/vlabs-dev/labs/cglab/experiments/dda-algorithm-pvg/theory.html "click here to get to this experiment")
## Digital Differential Analyser (DDA) Line Drawing Algorithm
---



### Theory


- DDA is an incremental scan conversion method to determine points on screen to draw a line where the Start and End coordinates of the Line segment are provided.

- DDA calculates the length of the line segment with respect to the difference between either X coordinates or Y coordinates, whichever is greater.

- In DDA, we either step across X-Direction and solve for Y (In case of gentle slope lines ) or we step Y-Direction and solve for X (incase of sharp slope lines) with the help of increment in either X and/or Y directions.

- As the increments are calculated with respect to X or Y direction ,so one of the increment will be either (1,/0/-1) and the other increment may be in float.

- Floating point arithmetic in DDA algorithm is time-consuming which results in poor end point accuracy.

- It is the simplest algorithm and does not require special skills for implementation.

|Algorithm       |Complexity     
| ------------- |:-------------
| __Bresenham's__      | __*O(n)*__

![DDA](computer-graphics-dda-algorithm6.jpg)
  



